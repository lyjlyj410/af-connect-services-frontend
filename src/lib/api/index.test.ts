import { describe, expect, it, vi } from 'vitest'
import client from '.'

const { mockCreateClient } = vi.hoisted(() => ({
  mockCreateClient: vi.fn(() => vi.fn())
}))

vi.mock('openapi-fetch', () => ({
  default: mockCreateClient,
}));

describe('index', () => {
  it('should provide a client', () => {
    expect(mockCreateClient).toHaveBeenCalled()
    expect(client).toBeDefined()
  })
})