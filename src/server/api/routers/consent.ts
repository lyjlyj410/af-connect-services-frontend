import { z } from 'zod';
import { env } from '@app/env.mjs';
import { createTRPCRouter, privateProcedure } from '@app/server/api/trpc';
import { getLogger } from '@app/utils/logger';

const logger = getLogger("backend", { redact: ['headers.PISA_ID'] });

export function buildHeaders(userSsn: string) {
  return {
    'Content-Type': 'application/json',
    'PISA_ID': userSsn
  }
}

export const consentRouter = createTRPCRouter({
  giveConsent: privateProcedure
    .input(z.object({ clientId: z.string(), scope: z.string(), foreignUserId: z.string() }))
    .mutation(async ({ ctx, input }) => {
      const url = `${env.SERVICE_URL}/user/giveConsent2?clientId=${input.clientId}&scope=${input.scope}&axaId=${input.foreignUserId}`;
      const headers = buildHeaders(ctx.user);
      logger.debug({ url, headers }, 'Providing consent.')

      const response = await fetch(url, { headers });
      const json: unknown = await response.json();

      const okResponseSchema = z.object({ authCode: z.string(), clientId: z.string(), enabled: z.boolean(), name: z.string(), redirectUrl: z.string().url() });
      const errorResponseSchema = z.object({ message: z.string(), exceptionId: z.string().optional() })

      const okResponse = okResponseSchema.safeParse(json);

      if (response.ok && okResponse.success) {
        const message = 'OK';
        return {
          message,
          redirectUrl: `${okResponse.data.redirectUrl}?authCode=${okResponse.data.authCode}&state=${input.foreignUserId}&message=OK`,
        };
      }

      const errorResponse = errorResponseSchema.safeParse(json);

      if (response.status === 404 && errorResponse.success && errorResponse.data.message === "Person not registered in AIS") {
        const message = 'Not possible at this time'
        logger.info({ url, headers, status: response.status, statusText: response.statusText, responseFromBackend: json }, message)

        return { message }
      }

      const message = 'Failed to provide consent, invalid response from back end.';
      logger.error({ url, headers, status: response.status, statusText: response.statusText, responseFromBackend: json }, message)
      return { message };
    }),

  revokeConsent: privateProcedure
    .input(z.object({ clientId: z.string(), scope: z.string(), foreignUserId: z.string() }))
    .mutation(async ({ ctx, input }) => {
      const url = `${env.SERVICE_URL}/user/revokeConsent2?clientId=${input.clientId}&scope=${input.scope}&axaId=${input.foreignUserId}`;
      const headers = buildHeaders(ctx.user);
      logger.debug({ url, headers }, 'Revoking consent.')

      const response = await fetch(url, { headers });
      const json: unknown = await response.json();

      if (response.ok) {
        const responseSchema = z.object({ authCode: z.null(), redirectUrl: z.string().url() });
        const data = responseSchema.parse(json);

        return {
          message: 'OK',
          redirectUrl: `${data.redirectUrl}?authCode=&state=${input.foreignUserId}&message=Error`,
        };
      } else {
        const message = 'Failed to revoke consent, invalid response from back end.';
        logger.error({ url, headers, status: response.status, statusText: response.statusText, responseFromBackend: json }, message)
        return { message };
      }
    }),
});
