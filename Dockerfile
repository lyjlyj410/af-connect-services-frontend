# Install dependencies and build from source
FROM node:20.11.1-alpine3.19 as build

RUN apk add --no-cache git=2.43.0-r0 && \
  # Upgrade npm
  npm upgrade -g npm

WORKDIR /app

# Install node modules
COPY package.json package-lock.json .npmrc ./
RUN npm clean-install

COPY . .

ENV NEXT_TELEMETRY_DISABLED=1

RUN SKIP_ENV_VALIDATION=1 npm run build

# Production image, copy files and run
FROM node:20.11.1-alpine3.19

RUN apk add --no-cache curl tini=0.19.0-r2 && \
  adduser --system --uid 1001 nextjs

WORKDIR /app

ENV NODE_ENV=production \
  NEXT_TELEMETRY_DISABLED=1

# Copy NextJS build artifacts
COPY --from=build --chown=nextjs:node /app/public ./public
COPY --from=build --chown=nextjs:node /app/.next/standalone ./
COPY --from=build --chown=nextjs:node /app/.next/static ./.next/static
COPY --from=build --chown=nextjs:node /app/logging.config.ts ./

USER nextjs:node

EXPOSE 3000

ENV PORT 3000

ENTRYPOINT [ "/sbin/tini", "--" ]
CMD ["node", "server.js"]
